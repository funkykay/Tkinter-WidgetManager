"""Objectholder.py: loads tkinter widgets defined by a JSON template. it is able to register additional widgets with an unique name"""
__author__      = "Kay Donner"
__copyright__   = "Copyright 2017, ObjectHolder"

import json
import re
import tkinter as tk
from tkinter import ttk as ttk


#    ___   ____   ____    ___    __ ______  __ __   ___   _      ___      ___  ____
#   /   \ |    \ |    |  /  _]  /  ]      ||  |  | /   \ | |    |   \    /  _]|    \
#  |     ||  o  )|__  | /  [_  /  /|      ||  |  ||     || |    |    \  /  [_ |  D  )
#  |  O  ||     |__|  ||    _]/  / |_|  |_||  _  ||  O  || |___ |  D  ||    _]|    /
#  |     ||  O  /  |  ||   [_/   \_  |  |  |  |  ||     ||     ||     ||   [_ |    \
#  |     ||     \  `  ||     \     | |  |  |  |  ||     ||     ||     ||     ||  .  \
#   \___/ |_____|\____||_____|\____| |__|  |__|__| \___/ |_____||_____||_____||__|\_|
class ObjectHolder:

    _objectList = {}
    _regex = re.compile(r'(tkinter|Tkinter)\..*')

    #    ____  ___    ___
    #   /    ||   \  |   \
    #  |  o  ||    \ |    \
    #  |     ||  D  ||  D  |
    #  |  _  ||     ||     |
    #  |  |  ||     ||     |
    #  |__|__||_____||_____|
    @staticmethod
    def add(object, name):
        if not bool(ObjectHolder._regex.search(str(type(object))[8:-2])):
            raise TypeError("object is no tkinter object")
        elif not isinstance(name, str):
            raise TypeError("name is no string")
        else:
            if not name in ObjectHolder._objectList:
                ObjectHolder._objectList[name] = object
            else:
                raise NameError("name already exists")

    #    ____    ___ ______
    #   /    |  /  _]      |
    #  |   __| /  [_|      |
    #  |  |  ||    _]_|  |_|
    #  |  |_ ||   [_  |  |
    #  |     ||     | |  |
    #  |___,_||_____| |__|
    @staticmethod
    def get(name):
        return ObjectHolder._objectList[name]

    @staticmethod
    def get_all_names_as_list_of_strings():
        returnlist = []
        for key in ObjectHolder._objectList:
            returnlist.append(key)
        return returnlist

    def __str__(self):
        printstring = ''
        for key in self._objectList:
            printstring += ('\'' + key + '\' : ' + str(type(self._objectList[key])) + '; ')
        return  printstring[:-1]

    #    ____  _____  ___   ____       ____   __ __  ____  _      ___
    #   |    |/ ___/ /   \ |    \     |    \ |  |  ||    || |    |   \
    #   |__  (   \_ |     ||  _  |    |  o  )|  |  | |  | | |    |    \
    #   __|  |\__  ||  O  ||  |  |    |     ||  |  | |  | | |___ |  D  |
    #  /  |  |/  \ ||     ||  |  |    |  O  ||  :  | |  | |     ||     |
    #  \  `  |\    ||     ||  |  |    |     ||     | |  | |     ||     |
    #   \____j \___| \___/ |__|__|    |_____| \__,_||____||_____||_____|
    @staticmethod
    def buildUpJsonLayout(path=None):
        if path is None:
            path = 'template.json'
            
        with open(path) as data_file:
            data = json.load(data_file)
                
        ObjectHolder.add(tk.Tk(), data["ID"])

        ObjectHolder._buildUpRecursive(data["elements"], ObjectHolder.get(data["ID"]), data["insideLayout"])

    @staticmethod
    def _buildUpRecursive(jsonList, parent, parentLayout):

        #      __  ____     ___   ____  ______    ___       ___   ____   ____    ___    __ ______
        #     /  ]|    \   /  _] /    ||      |  /  _]     /   \ |    \ |    |  /  _]  /  ]      |
        #    /  / |  D  ) /  [_ |  o  ||      | /  [_     |     ||  o  )|__  | /  [_  /  /|      |
        #   /  /  |    / |    _]|     ||_|  |_||    _]    |  O  ||     |__|  ||    _]/  / |_|  |_|
        #  /   \_ |    \ |   [_ |  _  |  |  |  |   [_     |     ||  O  /  |  ||   [_/   \_  |  |
        #  \     ||  .  \|     ||  |  |  |  |  |     |    |     ||     \  `  ||     \     | |  |
        #   \____||__|\_||_____||__|__|  |__|  |_____|     \___/ |_____|\____||_____|\____| |__|
        for element in jsonList:
            if element["type"] == "Frame":
                addingObjectName = element["ID"]
                ObjectHolder.add(tk.Frame(parent), addingObjectName)

            elif element["type"] == "PanedWindow":
                addingObjectName = element["ID"]
                ObjectHolder.add(ttk.PanedWindow(parent, orient=tk.VERTICAL), addingObjectName)

            elif element["type"] == "Button":
                addingObjectName = element["ID"]
                buttontext = ''
                try:
                    buttontext = element["attributes"]["text"]
                except KeyError:
                    pass
                ObjectHolder.add(ttk.Button(parent, text=buttontext), addingObjectName)

            elif element["type"] == "Label":
                addingObjectName = element["ID"]
                try:
                    labeltext = element["attributes"]["text"]
                    ObjectHolder.add(tk.Label(parent, text=labeltext), addingObjectName)
                except KeyError:
                    pass

            elif element["type"] == "Text":
                try:
                    try:
                        addingObjectName = \
                            ObjectHolder.addScrollbarToObject(element["ID"], element["type"], element['scrollbar'], parent, element['attributes'])
                    except KeyError:
                        addingObjectName = \
                            ObjectHolder.addScrollbarToObject(element["ID"], element["type"], element['scrollbar'], parent, None)
                except KeyError:
                    addingObjectName = element["ID"]
                    ObjectHolder.add(tk.Text(parent), addingObjectName)

            elif element["type"] == "Treeview_ttk":
                try:
                    addingObjectName = \
                        ObjectHolder.addScrollbarToObject(element["ID"], element["type"], element['scrollbar'], parent, None)
                except KeyError:
                    addingObjectName = element["ID"]
                    ObjectHolder.add(ttk.Treeview(parent), addingObjectName)

            #   ____  ____   _____   ___  ____  ______       ___   ____   ____    ___    __ ______
            #  |    ||    \ / ___/  /  _]|    \|      |     /   \ |    \ |    |  /  _]  /  ]      |
            #   |  | |  _  (   \_  /  [_ |  D  )      |    |     ||  o  )|__  | /  [_  /  /|      |
            #   |  | |  |  |\__  ||    _]|    /|_|  |_|    |  O  ||     |__|  ||    _]/  / |_|  |_|
            #   |  | |  |  |/  \ ||   [_ |    \  |  |      |     ||  O  /  |  ||   [_/   \_  |  |
            #   |  | |  |  |\    ||     ||  .  \ |  |      |     ||     \  `  ||     \     | |  |
            #  |____||__|__| \___||_____||__|\_| |__|       \___/ |_____|\____||_____|\____| |__|
            try:
                if parentLayout == "pack":
                    ObjectHolder.get(addingObjectName).pack(side = element["outsideLayout"]["side"],
                                                         expand = element["outsideLayout"]["expand"],
                                                         fill = element["outsideLayout"]["fill"],
                                                         pady=1,
                                                         padx = 1)
                elif parentLayout == "grid":
                    stickyness = ''
                    try:
                        stickyness = element["outsideLayout"]["sticky"]
                    except KeyError:
                        pass
                    ObjectHolder.get(addingObjectName).grid(column= element["outsideLayout"]["column"],
                                                         row = element["outsideLayout"]["row"],
                                                         sticky = stickyness)
                elif parentLayout == "add":
                    adding_obj = ObjectHolder.get(addingObjectName)
                    adding_obj.master.add(adding_obj)

            except KeyError:
                pass

            try:
                ObjectHolder._buildUpRecursive(element["elements"], ObjectHolder.get(element["ID"]), element["insideLayout"])
            except KeyError:
                pass

    @staticmethod
    def addScrollbarToObject(name, type, side, parent, attributes):
        holderframename = name + '_fr'
        ObjectHolder.add(tk.Frame(parent, bd=3, relief=tk.SUNKEN), holderframename)

        if type == "Treeview_ttk":
            ObjectHolder.add(ttk.Treeview(ObjectHolder.get(holderframename)), name)
        elif type == "Text":
            if attributes == None:
                ObjectHolder.add(tk.Text(ObjectHolder.get(holderframename)), name)
            else:
                ObjectHolder.add(tk.Text(ObjectHolder.get(holderframename),
                                                       ObjectHolder.current_modul,
                                                       wrap=attributes['wrap'],
                                                       font=(attributes['font'][0], attributes['font'][1]))
                                 , name)

        if not side == 'both':
            if side == 'right' or side == 'left':
                ObjectHolder.add(
                    ttk.Scrollbar(
                        ObjectHolder.get(holderframename),
                        orient="vertical",
                        command=ObjectHolder.get(name).yview
                    ),
                    name + "_sb"
                )
                ObjectHolder.get(name + "_sb").pack(side=side, expand=0, fill='y')

            elif side == 'bottom' or side == 'top':
                ObjectHolder.add(
                    ttk.Scrollbar(
                        ObjectHolder.get(holderframename),
                        orient="horizontal",
                        command=ObjectHolder.get(name).xview
                    ),
                    name + "_sb"
                )
                ObjectHolder.get(name + "_sb").pack(side=side, expand=0, fill='x')
            ObjectHolder.get(name).configure(yscrollcommand=ObjectHolder.get(name + "_sb").set)

            if side == 'left':
                side = 'right'
            elif side == 'right':
                side = 'left'
            elif side == 'bottom':
                side = 'top'
            else:
                side = 'bottom'

            ObjectHolder.get(name).pack(side=side, expand=1, fill='both')

        else:
            ObjectHolder.add(ttk.Scrollbar(ObjectHolder.get(holderframename),
                                           orient="vertical",
                                           command=ObjectHolder.get(name).yview),
                             name + "_sb_v")

            ObjectHolder.add(ttk.Scrollbar(ObjectHolder.get(holderframename),
                                           orient="horizontal",
                                           command=ObjectHolder.get(name).xview),
                             name + "_sb_h")

            ObjectHolder.get(name).configure(yscrollcommand=ObjectHolder.get(name + "_sb_v").set)
            ObjectHolder.get(name).configure(xscrollcommand=ObjectHolder.get(name + "_sb_h").set)

            ObjectHolder.get(name).grid(column=0, row=0, sticky='news')
            ObjectHolder.get(name + "_sb_v").grid(column=1, row=0, sticky='news')
            ObjectHolder.get(name + "_sb_h").grid(column=0, row=1, sticky='news')
            ObjectHolder.get(holderframename).columnconfigure(0, weight=1)
            ObjectHolder.get(holderframename).columnconfigure(1, weight=0)
            ObjectHolder.get(holderframename).rowconfigure(0, weight=1)
            ObjectHolder.get(holderframename).rowconfigure(1, weight=0)

        return holderframename

if __name__ == "__main__":
    ObjectHolder.buildUpJsonLayout('template.json')

    treeview = ObjectHolder.get('Display')

    list_of_ints = [1, 2, 3, 4]
    for i in list_of_ints:
        treeview.insert("", 0, text=str(i))

    text = ObjectHolder.get('TextView')

    text.insert(
        tk.END,
        '''
        Tkinter is Python's de-facto standard GUI (Graphical User Interface) package.
        It is a thin object-oriented layer on top of Tcl/Tk.

        Tkinter is not the only GuiProgramming toolkit for Python.
        It is however the most commonly used one.
        CameronLaird calls the yearly decision to keep TkInter "one of the minor traditions of the Python world."

        The Tkinter wiki: http://tkinter.unpythonic.net/wiki/
        '''
    )

    ObjectHolder.get("MainWindow").mainloop()


