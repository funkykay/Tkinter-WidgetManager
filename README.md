# Tkinter-WidgetManager
A python class that makes tkinter gui development more comfortable. The class stores widgets in one domain. For registration you need a unique name for each widget. They can be accessed by calling theire given name.

The WidgetManager is able to load Widgets with a JSON-file. See Example for more details.

An example is at the main method at the end of the ObjectHolder.py file or see (NOTE: To understand where the names come from, look at the JSON-template):

```python
def main():
    ObjectHolder.buildUpJsonLayout()

    treeview = ObjectHolder.get('Display')

    list_of_ints = [1, 2, 3, 4]
    for i in list_of_ints:
        treeview.insert("", 0, text=str(i))

    text = ObjectHolder.get('TextView')

    text.insert(
        tk.END,
        '''
        Tkinter is Python's de-facto standard GUI (Graphical User Interface) package.
        It is a thin object-oriented layer on top of Tcl/Tk.

        Tkinter is not the only GuiProgramming toolkit for Python.
        It is however the most commonly used one.
        CameronLaird calls the yearly decision to keep TkInter 
        "one of the minor traditions of the Python world."

        The Tkinter wiki: http://tkinter.unpythonic.net/wiki/
        '''
    )

    ObjectHolder.get("MainWindow").mainloop()
```